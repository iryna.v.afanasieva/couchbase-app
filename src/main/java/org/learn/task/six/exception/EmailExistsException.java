package org.learn.task.six.exception;

/**
 * Email Exists Exception.
 */
public class EmailExistsException extends RuntimeException {
  public EmailExistsException(String email) {
    super(email);
  }
}
