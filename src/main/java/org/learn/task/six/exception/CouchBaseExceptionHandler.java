package org.learn.task.six.exception;

import com.couchbase.client.core.error.UserNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.learn.task.six.dto.ErrorDto;
import org.springframework.data.couchbase.transaction.error.TransactionSystemUnambiguousException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Controller Advice.
 */
@Slf4j
@ControllerAdvice
public class CouchBaseExceptionHandler {
  /**
   * Handle exceptions.
   *
   * @param exception exception.
   *
   * @return ResponseEntity.
   */
  @ExceptionHandler({TransactionSystemUnambiguousException.class})
  public ResponseEntity<ErrorDto> handleException(TransactionSystemUnambiguousException exception) {
    log.error(exception.getMessage(), exception);
    return new ResponseEntity<>(
        ErrorDto.builder()
            .code(HttpStatus.BAD_REQUEST.value())
            .message(exception.getMessage()).build(),
        HttpStatus.BAD_REQUEST);
  }

  /**
   * Handle exceptions.
   *
   * @param exception exception.
   *
   * @return ResponseEntity.
   */
  @ExceptionHandler({UserNotFoundException.class})
  public ResponseEntity<ErrorDto> handleUserNotFoundException(UserNotFoundException exception) {
    log.error(exception.getMessage(), exception);
    return new ResponseEntity<>(
        ErrorDto.builder()
            .code(HttpStatus.NOT_FOUND.value())
            .message(exception.getMessage()).build(),
        HttpStatus.NOT_FOUND);
  }

  /**
   * Handle exceptions.
   *
   * @param exception exception.
   *
   * @return ResponseEntity.
   */
  @ExceptionHandler({RuntimeException.class})
  public ResponseEntity<ErrorDto> handleRuntimeException(RuntimeException exception) {
    log.error(exception.getMessage(), exception);
    return new ResponseEntity<>(
        ErrorDto.builder()
            .code(HttpStatus.I_AM_A_TEAPOT.value())
            .message("It happens sometimes").build(),
        HttpStatus.I_AM_A_TEAPOT);
  }
}
