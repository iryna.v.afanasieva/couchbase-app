package org.learn.task.six.configuration;

import com.couchbase.client.core.msg.kv.DurabilityLevel;
import com.couchbase.client.java.env.ClusterEnvironment;
import com.couchbase.client.java.query.QueryScanConsistency;
import com.couchbase.client.java.transactions.config.TransactionsConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.CouchbaseClientFactory;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.config.BeanNames;
import org.springframework.data.couchbase.core.mapping.event.ValidatingCouchbaseEventListener;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;
import org.springframework.data.couchbase.transaction.CouchbaseCallbackTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * Couchbase Configuration.
 */
@Configuration
@EnableCouchbaseRepositories(basePackages = {"org.learn.task.six"})
@EnableTransactionManagement
public class CouchbaseConfiguration extends AbstractCouchbaseConfiguration {

  public static final String NODE_LIST = "localhost";
  public static final String BUCKET_NAME = "app";
  public static final String BUCKET_USERNAME = "Administrator";
  public static final String BUCKET_PASSWORD = "administrator";

  @Override
  public String getConnectionString() {
    return NODE_LIST;
  }

  @Override
  public String getUserName() {
    return BUCKET_USERNAME;
  }

  @Override
  public String getPassword() {
    return BUCKET_PASSWORD;
  }

  @Override
  public String getBucketName() {
    return BUCKET_NAME;
  }

  @Override
  public void configureEnvironment(ClusterEnvironment.Builder builder) {
    builder.transactionsConfig(TransactionsConfig.durabilityLevel(DurabilityLevel.NONE));
  }

  @Bean(BeanNames.COUCHBASE_TRANSACTION_MANAGER)
  public CouchbaseCallbackTransactionManager couchbaseTransactionManager(
      CouchbaseClientFactory clientFactory) {
    return new CouchbaseCallbackTransactionManager(clientFactory);
  }

  @Override
  protected boolean autoIndexCreation() {
    return true;
  }

  @Override
  public QueryScanConsistency getDefaultConsistency() {
    return QueryScanConsistency.REQUEST_PLUS;
  }

  @Bean
  public LocalValidatorFactoryBean localValidatorFactoryBean() {
    return new LocalValidatorFactoryBean();
  }

  @Bean
  public ValidatingCouchbaseEventListener validatingCouchbaseEventListener() {
    return new ValidatingCouchbaseEventListener(localValidatorFactoryBean());
  }
}