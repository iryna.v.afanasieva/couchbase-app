package org.learn.task.six.controller;

import java.util.List;
import lombok.AllArgsConstructor;
import org.learn.task.six.dto.UserDto;
import org.learn.task.six.service.FullTextSearchService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Full Text Search Controller.
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/search")
public class FullTextController {

  private final FullTextSearchService fullTextSearchService;

  @GetMapping("/user")
  public List<UserDto> getByFullSearch(@RequestParam String q) {
    return fullTextSearchService.searchByText(q);
  }

}
