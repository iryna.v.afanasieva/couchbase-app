package org.learn.task.six.controller;

import jakarta.validation.Valid;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.learn.task.six.dto.SportDto;
import org.learn.task.six.dto.UserDto;
import org.learn.task.six.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * User Controller.
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/user")
public class UserController {
  private final UserService userService;

  @GetMapping("/{id}")
  public UserDto getUser(@PathVariable String id) {
    return userService.findUserById(id);
  }

  @GetMapping("/email/{email}")
  public UserDto getUserByEmail(@PathVariable String email) {
    return userService.findUserByEmail(email);
  }

  @GetMapping("/sport/{sportTitle}")
  public List<UserDto> getAllUsersBySportTitle(@PathVariable String sportTitle) {
    return userService.findAllBySport(sportTitle);
  }

  @PostMapping
  public UserDto postUser(@Valid @RequestBody UserDto user) {
    final var newUser = user.toBuilder().id(UUID.randomUUID().toString()).build();
    return userService.createUser(newUser);
  }

  @PostMapping("/{id}/sport")
  public UserDto addUserSport(@PathVariable String id, @Valid @RequestBody SportDto sportDto) {
    final var newSportDto = sportDto.toBuilder().id(UUID.randomUUID().toString()).build();
    return userService.addSport(id, newSportDto);
  }
}
