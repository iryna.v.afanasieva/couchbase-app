package org.learn.task.six.repository;

import java.util.List;
import java.util.Optional;
import org.learn.task.six.dto.UserDto;
import org.springframework.data.couchbase.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * User Repository.
 */
@Repository
public interface UserRepository extends CrudRepository<UserDto, String> {

  UserDto save(UserDto entity);

  Optional<UserDto> findById(String id);

  Optional<UserDto> findByEmail(String email);

  @Query("""
      #{#n1ql.selectEntity}
      WHERE ANY sportTitle IN sports[*].sportTitle SATISFIES sportTitle = $1 end""")
  List<UserDto> findBySports_SportTitle(String sportTitle);

}
