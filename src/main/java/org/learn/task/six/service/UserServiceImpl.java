package org.learn.task.six.service;

import com.couchbase.client.core.error.UserNotFoundException;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import org.learn.task.six.dto.SportDto;
import org.learn.task.six.dto.UserDto;
import org.learn.task.six.exception.EmailExistsException;
import org.learn.task.six.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User Service.
 */
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
  private final UserRepository repository;


  @Override
  @Transactional
  public UserDto createUser(UserDto user) {
    final var email = user.getEmail();
    if (repository.findByEmail(email).isEmpty()) {
      return repository.save(user);
    } else {
      throw new EmailExistsException(email);
    }
  }

  @Override
  public UserDto findUserById(String id) {
    return repository.findById(id).orElseThrow(() -> new UserNotFoundException("app", "id"));
  }

  @Override
  public UserDto findUserByEmail(String email) {
    return repository.findByEmail(email)
        .orElseThrow(() -> new UserNotFoundException("app", "email"));
  }

  @Override
  public UserDto addSport(String userId, SportDto sportDto) {
    final var user = this.findUserById(userId);
    if (user.getSports() == null) {
      user.setSports(new ArrayList<>());
    }
    user.getSports().add(sportDto);
    return repository.save(user);
  }

  @Override
  public List<UserDto> findAllBySport(String sportTitle) {
    return repository.findBySports_SportTitle(sportTitle);
  }

}
