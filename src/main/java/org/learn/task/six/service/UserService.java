package org.learn.task.six.service;

import java.util.List;
import org.learn.task.six.dto.SportDto;
import org.learn.task.six.dto.UserDto;

/**
 * User Service.
 */
public interface UserService {
  UserDto createUser(UserDto user);

  UserDto findUserById(String id);

  UserDto findUserByEmail(String email);

  UserDto addSport(String userId, SportDto sportDto);

  List<UserDto> findAllBySport(String sportTitle);
}
