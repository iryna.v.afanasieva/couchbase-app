package org.learn.task.six.service;

import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.search.SearchQuery;
import com.couchbase.client.java.search.result.SearchResult;
import com.couchbase.client.java.search.result.SearchRow;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.learn.task.six.dto.UserDto;
import org.springframework.stereotype.Service;

/**
 * Full Text Search Service.
 */
@Service
@Slf4j
@AllArgsConstructor
public class FullTextSearchService {

  private final UserService userService;
  private final Cluster cluster;

  /**
   * Search all users by query.
   *
   * @param query to search
   * @return list of Users
   */
  public List<UserDto> searchByText(String query) {
    try {
      final SearchResult result = cluster
          .searchQuery("FullText", SearchQuery.queryString(query));
      return result.rows().stream().map(SearchRow::id).map(userService::findUserById).toList();
    } catch (Exception ex) {
      log.error("ups", ex);
    }
    return List.of();
  }
}
