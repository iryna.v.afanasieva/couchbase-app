package org.learn.task.six.enumeration;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;

/**
 * Gender.
 */
public enum Gender {
  FEMALE,
  MALE,
  @JsonEnumDefaultValue
  UNKNOWN
}
