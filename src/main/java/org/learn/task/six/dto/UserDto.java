package org.learn.task.six.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;
import org.learn.task.six.enumeration.Gender;

/**
 * UserDto.
 */
@Data
@Builder(toBuilder = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Jacksonized
public class UserDto {
  String id;
  @NotNull
  String email;
  @NotNull
  String fullName;
  @NotNull
  LocalDate birthDate;
  @NotNull
  Gender gender;
  List<SportDto> sports;
}
