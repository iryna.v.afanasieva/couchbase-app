package org.learn.task.six.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Error Dto.
 */
@Data
@Builder(toBuilder = true)
public class ErrorDto {
  int code;
  String message;
}
