package org.learn.task.six.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

/**
 * SportDto.
 */
@Data
@Builder(toBuilder = true)
@Jacksonized
public class SportDto {
  String id;
  @NotNull
  String sportTitle;
  @NotNull
  String sportProficiency;
}
