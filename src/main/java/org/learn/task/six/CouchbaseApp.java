package org.learn.task.six;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Boot Application.
 */
@SpringBootApplication
public class CouchbaseApp {

  /**
   * Main SpringBoot application start.
   *
   * @param args parameters
   */
  public static void main(String[] args) {
    SpringApplication.run(CouchbaseApp.class, args);
  }
}
